import { mount } from '@vue/test-utils'
import Home from '@/views/Home.vue'

test('Check Home', () => {
  const wrapper = mount(Home, { shallow: true })

  wrapper.setData({ name: 'This is an example task.' })

  expect(wrapper.find('.error').exists()).toBe(false)
})
